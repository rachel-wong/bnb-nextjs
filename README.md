## Backend side to a BnB-nextJS app

> The purpose of this app is to experiment with nextJS and sanity IO content modelling. **UI was not the focus, rather on linking correct content modelling to react.**

To run `sanity start`

### Sanity notes
* sanity init -> creates a new sanity project.
* sanity start -> runs or starts up the project. Then can access the backend http://localhost:3333


### Content Modelling notes
* Be mindful of GROQ syntax
* "document" type is publishable
* Use `references` to refer to a "parent" type as much as possible. In this case, `person` is a parent to `host` and `traveller`. This means that both can share same fields
* `hotspot` can be used on images to allow for further cropping and formatting options.
* when content modelling, use custom types wherever needed. For example, an array of `images` should have a custom type of `propertyImage`.