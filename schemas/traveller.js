export default {
  name: 'traveller',
  title: 'Traveller',
  type: 'reference', // refers to a person
  to: [{
    type: 'person'
  }]
}