export default {
  name: 'person',
  title: 'Person',
  type: 'document', // can be a host, can be a traveller
  fields: [
    {
      name: 'name',
      title: 'Name',
      type: 'string',
      description: 'First name, last name format'
    },
    {
      name: 'slug',
      title: 'Slug',
      type: 'slug',
      options: {
        source: 'name',
        maxLength: 100
      },
    },
    {
      name: 'id',
      title: 'ID',
      type: 'number'
    },
    {
      name: 'image',
      title: 'Image',
      type: 'image',
      options: {
        hotspot: true,
      }
    }
  ],
  // this specifies how it appears in the backend listview
  preview: {
    select: {
      title: 'name',
      media: 'image'
    }
  }
}